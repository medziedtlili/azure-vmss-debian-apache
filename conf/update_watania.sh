#!/bin/bash

SITES_HOME="/mnt/nfs/"
ACTION_FILE="${SITES_HOME}html/.UPDATE.DO"
CHECK_FILE="/usr/local/share/.lastrun_watania"

if [ ! -f $ACTION_FILE ]; then
  # no update is required
  exit 0
fi

if [ ! -f $CHECK_FILE ]; then
  # create the check file if it does not exist
  date '+%s' > $CHECK_FILE
fi

last_updated=$(cat $CHECK_FILE)
last_action=$( stat --printf='%Y' ${ACTION_FILE} )

diff=$(( $last_action - $last_updated ))
if [ $diff -gt 1 ]; then
    # DO the update 
    rsync -iauv --progress --no-perms --no-owner --no-group --exclude='.UPDATE.DO' --exclude='sites/default/settings.php' --exclude='sites/default/files/' ${SITES_HOME}/html/ /var/www/html/
    echo "$last_action" > $CHECK_FILE
    echo "$(date) $(hostname) WATANIA updated." >> /mnt/nfs/updates.log
fi


