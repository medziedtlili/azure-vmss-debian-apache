#!/bin/bash

# The MIT License (MIT)
#
# Copyright (c) 2017 MediaNET
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


# Variables - Initialize default values
DBHOST=""
DBNAMES=(watania ttnews ttsports akhbar_dar)
USERNAMES=(uwatania uttnews uttsports uakhbardar)
PASSWORDS=(duoG123Y X4VEBhC9 X4VEBhC9 maTA7XSS)

# Parse script parameters
while getopts "d:" optname; do
 echo $optname
  case $optname in
  d) # DB host
    DBHOST=${OPTARG}
    ;;
  esac
done

log()
{
	echo "$1"
}

log "Begin execution of VM configuration script extension on ${HOSTNAME}"

if [ "${UID}" -ne 0 ];
then
    log "Script executed without root permissions"
    echo "You must be root to run this program." >&2
    exit 3
fi


install_required_packages()
{
  # Install required packages, install needed bits in a loop because a lot of installs 
  # happen on VM init, so won't be able to grab the dpkg lock immediately.
  echo "installing required packages"
  sed -i 's/ main/ main contrib non-free /g' /etc/apt/sources.list
  apt-get -y update
  until DEBIAN_FRONTEND=noninteractive apt-get -y install apache2 libapache2-mod-php5 php5 php5-gd php5-mysql php5-mcrypt php5-curl libapache2-mod-fastcgi php5-fpm mysql-client varnish php5-curl cifs-utils curl nfs-common ganglia-monitor logtail make unzip bc sysstat rsync 
  do
  log "installing required packages....."
  sleep 2
  done
  
  
  # Install Drush
  php -r "readfile('http://files.drush.org/drush.phar');" > drush
  chmod +x drush
  mv drush /usr/local/bin
  
  # Install Composer
  curl -sS https://getcomposer.org/installer | php
  mv composer.phar /usr/local/bin/composer
}


configure_prequisites()
{
  log "configuring prerquisites"
  
 # uncomments lines below to display errors
   #  sed -i 's/display_errors = Off/display_errors = On/' /etc/php5/apache2/php.ini
   #  sed -i 's/display_errors = Off/display_errors = On/' /etc/php5/cli/php.ini
 
 # Set overrides on in apache2.conf
 #sed -i 's/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf
 sed -i 's/Listen 80/Listen 8080/g' /etc/apache2/ports.conf
 
 # Copy config files
 cp 000-default.conf /etc/apache2/sites-available/ 
 cp 15-portail.tunisiatv.tn.conf /etc/apache2/sites-available/ 
 cp 20-sport.tunisiatv.tn.conf /etc/apache2/sites-available/ 
 cp 25-akhbareddar.tunisiatv.tn.conf /etc/apache2/sites-available/ 
 cp 30-news.tunisiatv.tn.conf /etc/apache2/sites-available/ 
 cp php-fpm.conf /etc/php5/fpm/
 wget --no-check-certificate https://bitbucket.org/medziedtlili/azure-vmss-debian-apache/raw/master/conf/apache/fpmwww.conf
 wget --no-check-certificate https://bitbucket.org/medziedtlili/azure-vmss-debian-apache/raw/master/scripts/service_checker.sh
 cp fpmwww.conf /etc/php5/fpm/pool.d/www.conf
 cp apache2.conf /etc/apache2/
 cp security.conf /etc/apache2/conf-available/ 
 cp fastcgi.conf /etc/apache2/mods-available/
 
 a2enmod rewrite actions alias fastcgi 
 a2dismod php5 
 a2ensite 15-portail.tunisiatv.tn 
 a2ensite 20-sport.tunisiatv.tn 
 a2ensite 25-akhbareddar.tunisiatv.tn 
 a2ensite 30-news.tunisiatv.tn 
 
 # create nfs mount point (sites home)
 mkdir -p /mnt/nfs
 
 # mount nfs files system 
 sudo mount ${DBHOST}:/var/www/nfs_sites /mnt/nfs
 
 # fstab
 echo '' >> /etc/fstab
 echo $DBHOST':/var/www/nfs_sites /mnt/nfs   nfs  rw,sync,hard,intr  0  0 ' >> /etc/fstab
 echo '' >> /etc/fstab
 
}

configure_varnish()
{
  log "configuring Varnish"
  
  echo "d5f5c818-9929-48ec-b51c-a4e26bcc1aff" > /etc/varnish/secret
  cp default.vcl /etc/varnish/ 
  cp varnish.service /etc/systemd/system/ 
  cp varnishncsa.service /etc/systemd/system/  
  
  systemctl daemon-reload
}

install_site()
{
  cp -apr /mnt/nfs/* /var/www/
  chown -R www-data /var/www/
  # mount "files" dirs.
  mkdir -p /var/www/{html,news.tunisiatv.tn,sport.tunisiatv.tn,akhbareddar.tunisiatv.tn}/sites/default/files
  sudo mount ${DBHOST}:/var/www/nfs_watania_files /var/www/html/sites/default/files
  sudo mount ${DBHOST}:/var/www/nfs_news_files /var/www/news.tunisiatv.tn/sites/default/files
  sudo mount ${DBHOST}:/var/www/nfs_sport_files /var/www/sport.tunisiatv.tn/sites/default/files
  sudo mount ${DBHOST}:/var/www/nfs_akhbareddar_files /var/www/akhbareddar.tunisiatv.tn/sites/default/files
  
  echo '' >> /etc/fstab
  echo $DBHOST':/var/www/nfs_watania_files /var/www/html/sites/default/files   nfs  rw,sync,hard,intr  0  0 ' >> /etc/fstab
  echo $DBHOST':/var/www/nfs_news_files /var/www/news.tunisiatv.tn/sites/default/files   nfs  rw,sync,hard,intr  0  0 ' >> /etc/fstab
  echo $DBHOST':/var/www/nfs_sport_files /var/www/sport.tunisiatv.tn/sites/default/files   nfs  rw,sync,hard,intr  0  0 ' >> /etc/fstab
  echo $DBHOST':/var/www/nfs_akhbareddar_files /var/www/akhbareddar.tunisiatv.tn/sites/default/files   nfs  rw,sync,hard,intr  0  0 ' >> /etc/fstab
  echo '' >> /etc/fstab
}


configure_site()
{
  # Watania
  sed -i -E "s/database' => '(.*)'/database' => '${DBNAMES[0]}'/g" /var/www/html/sites/default/settings.php
  sed -i -E "s/username' => '(.*)'/username' => '${USERNAMES[0]}'/g" /var/www/html/sites/default/settings.php
  sed -i -E "s/password' => '(.*)'/password' => '${PASSWORDS[0]}'/g" /var/www/html/sites/default/settings.php
  sed -i -E "s/host' => '(.*)'/host' => '$DBHOST'/g" /var/www/html/sites/default/settings.php
  
  # TTnews
  sed -i -E "s/database' => '(.*)'/database' => '${DBNAMES[1]}'/g" /var/www/news.tunisiatv.tn/sites/default/settings.php
  sed -i -E "s/username' => '(.*)'/username' => '${USERNAMES[1]}'/g" /var/www/news.tunisiatv.tn/sites/default/settings.php
  sed -i -E "s/password' => '(.*)'/password' => '${PASSWORDS[1]}'/g" /var/www/news.tunisiatv.tn/sites/default/settings.php
  sed -i -E "s/host' => '(.*)'/host' => '$DBHOST'/g" /var/www/news.tunisiatv.tn/sites/default/settings.php
  
  # TTsports
  sed -i -E "s/database' => '(.*)'/database' => '${DBNAMES[2]}'/g" /var/www/sport.tunisiatv.tn/sites/default/settings.php
  sed -i -E "s/username' => '(.*)'/username' => '${USERNAMES[2]}'/g" /var/www/sport.tunisiatv.tn/sites/default/settings.php
  sed -i -E "s/password' => '(.*)'/password' => '${PASSWORDS[2]}'/g" /var/www/sport.tunisiatv.tn/sites/default/settings.php
  sed -i -E "s/host' => '(.*)'/host' => '$DBHOST'/g" /var/www/sport.tunisiatv.tn/sites/default/settings.php
  
  # AkhbarDar
  sed -i -E "s/database' => '(.*)'/database' => '${DBNAMES[3]}'/g" /var/www/akhbareddar.tunisiatv.tn/sites/default/settings.php
  sed -i -E "s/username' => '(.*)'/username' => '${USERNAMES[3]}'/g" /var/www/akhbareddar.tunisiatv.tn/sites/default/settings.php
  sed -i -E "s/password' => '(.*)'/password' => '${PASSWORDS[3]}'/g" /var/www/akhbareddar.tunisiatv.tn/sites/default/settings.php
  sed -i -E "s/host' => '(.*)'/host' => '$DBHOST'/g" /var/www/akhbareddar.tunisiatv.tn/sites/default/settings.php
  
  # CRON tasks
  cp update_akhbardar.sh /usr/local/bin/ && chmod a+rx /usr/local/bin/update_akhbardar.sh
  cp update_news.sh /usr/local/bin/ && chmod a+rx /usr/local/bin/update_news.sh
  cp update_sport.sh /usr/local/bin/ && chmod a+rx /usr/local/bin/update_sport.sh
  cp update_watania.sh /usr/local/bin/ && chmod a+rx /usr/local/bin/update_watania.sh
  
  echo '' > /etc/cron.d/update_sites
  echo '* * * * * root /usr/local/bin/update_akhbardar.sh' >> /etc/cron.d/update_sites
  echo '* * * * * root /usr/local/bin/update_news.sh' >> /etc/cron.d/update_sites
  echo '* * * * * root /usr/local/bin/update_sport.sh' >> /etc/cron.d/update_sites
  echo '* * * * * root /usr/local/bin/update_watania.sh' >> /etc/cron.d/update_sites
  echo '' >> /etc/cron.d/update_sites

}

configure_ganglia()
{
  log "Configuring Ganglia ...."
  sed -i '0,/mcast_join = 239.2.11.71/{s/mcast_join = 239.2.11.71/host = 10.1.0.5/}'  /etc/ganglia/gmond.conf
  sed -i '0,/mcast_join = 239.2.11.71/{s/mcast_join = 239.2.11.71//}'  /etc/ganglia/gmond.conf
  sed -i '0,/bind = 239.2.11.71/{s/bind = 239.2.11.71//}'  /etc/ganglia/gmond.conf
  sed -i 's/port = 8649/port = 8655/g' /etc/ganglia/gmond.conf
  sed -i -E '0,/name =/{s/name = "(.*)"/name = "WWWwatania"/}' /etc/ganglia/gmond.conf
  
  wget --no-check-certificate https://github.com/ganglia/ganglia_contrib/archive/master.zip --output-document=ganglia_contrib.zip
  unzip ganglia_contrib.zip
  cd ganglia_contrib-master/ganglia-logtailer/
  make install
  cd ../..
  
  echo "" > /etc/cron.d/ganglia
  echo "* * * * * root /usr/sbin/ganglia-logtailer --classname VarnishLogtailer --log_file /var/log/varnish/access.log --mode cron" >> /etc/cron.d/ganglia
  echo "" >> /etc/cron.d/ganglia
  cp VarnishLogtailer.py /usr/share/ganglia-logtailer/
  
  /etc/init.d/ganglia-monitor restart
  
  log ".... done."
}

restart_webserver()
{
  log "Restarting Web server ...."
  systemctl restart php5-fpm.service
  systemctl restart apache2.service 
  systemctl restart varnish.service 
  sleep 5 && systemctl restart varnishncsa.service 
  log ".... Web server restarted."
}

install_cron_jobs()
{
  log "Installing CRON jobs ...."
  cp service_checker.sh /usr/local/bin/ && chmod a+rx /usr/local/bin/service_checker.sh
  echo '' > /etc/cron.d/service_checker
  echo '* * * * * root /usr/local/bin/service_checker.sh' >> /etc/cron.d/service_checker
  echo '' >> /etc/cron.d/service_checker
  log ".... done."
}


# Step 1
install_required_packages

# Step 2
configure_prequisites

# Step 3
configure_varnish

# Step 4
install_site

# Step 5
configure_site

# Step 6
configure_ganglia

# Step 7
restart_webserver

# Step 8
install_cron_jobs


