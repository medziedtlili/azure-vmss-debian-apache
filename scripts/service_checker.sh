#!/bin/bash

services=( varnish php5-fpm apache2 varnishncsa )
load_limit=10.0
load=$( uptime | awk -F'[a-z]:' '{ print $2}' | awk -F\, '{print $1}' )
executed=0

restart_service () {
    /etc/init.d/$1 restart > /dev/null
}

if (( $(echo "$load > $load_limit" | bc -l) )); then
    restart_service php5-fpm
    restart_service apache2
    executed=1
fi


for i in "${services[@]}"
do
        /etc/init.d/$i status > /dev/null
        if [ $? -ne 0 ]
        then
                restart_service $i
                executed=1
        fi
done

lsof -Pi :80 -sTCP:LISTEN -t >/dev/null
if [ $? -ne 0 ]
then
    restart_service varnish
    executed=1
fi

/usr/bin/gmetric --type "int8" --name "Services_executed" --value $executed --unit "Boolean"
