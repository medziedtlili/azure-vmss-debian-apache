{
  "$schema": "http://schema.management.azure.com/schemas/2015-01-01/deploymentTemplate.json",
  "contentVersion": "1.0.0.0",
  "parameters": {
    "vmSku": {
      "type": "string",
      "defaultValue": "Standard_F2s",
      "metadata": {
        "description": "Size of VMs in the VM Scale Set."
      },
      "allowedValues": [
        "Standard_D1",
        "Standard_DS1",
        "Standard_D2",
        "Standard_DS2",
        "Standard_D3",
        "Standard_DS3",
        "Standard_D4",
        "Standard_DS4",
        "Standard_D11",
        "Standard_DS11",
        "Standard_D12",
        "Standard_DS12",
        "Standard_D13",
        "Standard_DS13",
        "Standard_D14",
        "Standard_DS14",
        "Standard_F1s",
        "Standard_F2s",
        "Standard_F4s"
      ]
    },
    "debianOSVersion": {
      "type": "string",
      "defaultValue": "8",
      "allowedValues": [
        "7",
        "8"
      ],
      "metadata": {
        "description": "The Ubuntu version for the Drupal VM. This will pick a fully patched image of this given Ubuntu version. Allowed values are: 14.04.4-LTS."
      }
    },
    
    
    "vmssName": {
      "type": "string",
      "metadata": {
        "description": "String used as a base for naming resources. Must be 3-61 characters in length and globally unique across Azure. A hash is prepended to this string for some resources, and resource-specific information is appended."
      },
      "minLength": 3,
      "maxLength": 61
    },
    "instanceCount": {
      "type": "int",
      "defaultValue": 1,
      "metadata": {
        "description": "Number of VM instances (minimum and default instance count). Atleast 2 are recommended for high availability."
      },
      "maxValue": 10
    },
    "maximumInstanceCount": {
      "type": "int",
      "defaultValue": 10,
      "metadata": {
        "description": "Maximum number of instances in the VM Scale Set."
      },
      "maxValue": 100
    },
    "allowAutoScale": {
      "type": "bool",
      "defaultValue": false,
      "allowedValues": [
        true,
        false
      ],
      "metadata": {
        "description": "Allow auto scale for VMSS."
      }
    },
    "adminUsername": {
      "type": "string",
      "metadata": {
        "description": "Admin username on all VMs."
      }
    },
    "adminPassword": {
      "type": "securestring",
      "metadata": {
        "description": "Admin password on all VMs."
      }
    },
    "databaseHost": {
      "type": "string",
      "metadata": {
        "description": "Database server IP."
      }
    },
    "templateLocation": {
      "type": "string",
      "defaultValue": "https://bitbucket.org/medziedtlili/azure-vmss-debian-apache/raw/master/",
      "metadata": {
        "description": "template file location",
        "artifactsBaseUrl": "Base URL of the Publisher Template gallery package"
      }
    },
    "storageAccountNamePrefix": {
      "type": "string",
      "defaultValue": "stoacc",
      "metadata": {
        "description": "Storage account name prefix for the cluster"
      }
    }
  },
  "variables": {
    "accountid": "[concat('/subscriptions/',subscription().subscriptionId,'/resourceGroups/',variables('diagnosticsStorageAccountResourceGroup'),'/providers/','Microsoft.Storage/storageAccounts/', variables('diagnosticsStorageAccountName'))]",
    "addressPrefix": "10.0.0.0/16",
    "bePoolName": "[concat(variables('namingInfix'), 'bepool')]",
    "diagnosticsStorageAccountName": "[concat(variables('uniqueStringArray')[0], variables('newStorageAccountSuffix'))]",
    "diagnosticsStorageAccountResourceGroup": "[resourceGroup().name]",
    "mnSubnetName": "vmsswatansubnet",
    "frontEndIPConfigID": "[concat(variables('lbID'),'/frontendIPConfigurations/loadBalancerFrontEnd')]",
    "imageReference": "[variables('osType')]",
    "insightsApiVersion": "2015-04-01",
    "ipConfigName": "[concat(variables('namingInfix'), 'ipconfig')]",
    "lbID": "[resourceId('Microsoft.Network/loadBalancers',variables('loadBalancerName'))]",
    "lbPoolID": "[concat(variables('lbID'),'/backendAddressPools/', variables('bePoolName'))]",
    "lbProbeID": "[concat(variables('lbID'),'/probes/tcpProbe')]",
    "loadBalancerName": "[concat(variables('namingInfix'), 'lb')]",
    "location": "[resourceGroup().location]",
    "longNamingInfix": "[toLower(parameters('vmssName'))]",
    "namingInfix": "[toLower(substring(concat(parameters('vmssName'), uniqueString(resourceGroup().id)), 0, 9))]",
    "natBackendPort": 22,
    "natEndPort": 50119,
    "natPoolName": "[concat(variables('namingInfix'), 'natpool')]",
    "natStartPort": 50000,
    "networkApiVersion": "2015-05-01-preview",
    "newStorageAccountSuffix": "[concat(variables('namingInfix'), 'sa')]",
    "nicName": "[concat(variables('namingInfix'), 'nic')]",
    "osType": {
      "publisher": "credativ",
      "offer": "debian",
      "sku": "[parameters('debianOSVersion')]",
      "version": "latest"
    },
    "sshKeyPath": "[concat('/home/',parameters('adminUsername'),'/.ssh/authorized_keys')]",
    "publicIPAddressID": "[resourceId('Microsoft.Network/publicIPAddresses',variables('publicIPAddressName'))]",
    "publicIPAddressName": "[concat(variables('namingInfix'), 'pip')]",
    "saCount": "[length(variables('uniqueStringArray'))]",
    "storageAccountType": "Standard_LRS",
    "storageApiVersion": "2015-06-15",
    "templateAPIVersion": "2015-01-01",
    "uniqueStringArray": ["[concat(uniqueString(concat(resourceGroup().id, variables('newStorageAccountSuffix'), '0')))]"],
    "vhdContainerName": "[concat(variables('namingInfix'), 'vhd')]",
    "virtualNetworkName": "vmsswatanvnet",
    "wadcfgxend": "[concat('\"><MetricAggregation scheduledTransferPeriod=\"PT1H\"/><MetricAggregation scheduledTransferPeriod=\"PT1M\"/></Metrics></DiagnosticMonitorConfiguration></WadCfg>')]",
    "wadcfgxstart": "[concat(variables('wadlogs'),variables('wadperfcounters1'),variables('wadperfcounters2'),'<Metrics resourceId=\"')]",
    "wadlogs": "<WadCfg><DiagnosticMonitorConfiguration>",
    "wadmetricsresourceid": "[concat('/subscriptions/',subscription().subscriptionId,'/resourceGroups/',resourceGroup().name ,'/providers/','Microsoft.Compute/virtualMachineScaleSets/',variables('namingInfix'))]",
    "wadperfcounters1": "<PerformanceCounters scheduledTransferPeriod=\"PT1M\"><PerformanceCounterConfiguration counterSpecifier=\"\\Memory\\AvailableMemory\" sampleRate=\"PT15S\" unit=\"Bytes\"><annotation displayName=\"Memory available\" locale=\"en-us\"/></PerformanceCounterConfiguration><PerformanceCounterConfiguration counterSpecifier=\"\\Memory\\PercentAvailableMemory\" sampleRate=\"PT15S\" unit=\"Percent\"><annotation displayName=\"Mem. percent available\" locale=\"en-us\"/></PerformanceCounterConfiguration><PerformanceCounterConfiguration counterSpecifier=\"\\Memory\\UsedMemory\" sampleRate=\"PT15S\" unit=\"Bytes\"><annotation displayName=\"Memory used\" locale=\"en-us\"/></PerformanceCounterConfiguration><PerformanceCounterConfiguration counterSpecifier=\"\\Memory\\PercentUsedMemory\" sampleRate=\"PT15S\" unit=\"Percent\"><annotation displayName=\"Memory percentage\" locale=\"en-us\"/></PerformanceCounterConfiguration><PerformanceCounterConfiguration counterSpecifier=\"\\Memory\\PercentUsedByCache\" sampleRate=\"PT15S\" unit=\"Percent\"><annotation displayName=\"Mem. used by cache\" locale=\"en-us\"/></PerformanceCounterConfiguration><PerformanceCounterConfiguration counterSpecifier=\"\\Processor\\PercentIdleTime\" sampleRate=\"PT15S\" unit=\"Percent\"><annotation displayName=\"CPU idle time\" locale=\"en-us\"/></PerformanceCounterConfiguration><PerformanceCounterConfiguration counterSpecifier=\"\\Processor\\PercentUserTime\" sampleRate=\"PT15S\" unit=\"Percent\"><annotation displayName=\"CPU user time\" locale=\"en-us\"/></PerformanceCounterConfiguration><PerformanceCounterConfiguration counterSpecifier=\"\\Processor\\PercentProcessorTime\" sampleRate=\"PT15S\" unit=\"Percent\"><annotation displayName=\"CPU percentage guest OS\" locale=\"en-us\"/></PerformanceCounterConfiguration><PerformanceCounterConfiguration counterSpecifier=\"\\Processor\\PercentIOWaitTime\" sampleRate=\"PT15S\" unit=\"Percent\"><annotation displayName=\"CPU IO wait time\" locale=\"en-us\"/></PerformanceCounterConfiguration>",
    "wadperfcounters2": "<PerformanceCounterConfiguration counterSpecifier=\"\\PhysicalDisk\\BytesPerSecond\" sampleRate=\"PT15S\" unit=\"BytesPerSecond\"><annotation displayName=\"Disk total bytes\" locale=\"en-us\"/></PerformanceCounterConfiguration><PerformanceCounterConfiguration counterSpecifier=\"\\PhysicalDisk\\ReadBytesPerSecond\" sampleRate=\"PT15S\" unit=\"BytesPerSecond\"><annotation displayName=\"Disk read guest OS\" locale=\"en-us\"/></PerformanceCounterConfiguration><PerformanceCounterConfiguration counterSpecifier=\"\\PhysicalDisk\\WriteBytesPerSecond\" sampleRate=\"PT15S\" unit=\"BytesPerSecond\"><annotation displayName=\"Disk write guest OS\" locale=\"en-us\"/></PerformanceCounterConfiguration><PerformanceCounterConfiguration counterSpecifier=\"\\PhysicalDisk\\TransfersPerSecond\" sampleRate=\"PT15S\" unit=\"CountPerSecond\"><annotation displayName=\"Disk transfers\" locale=\"en-us\"/></PerformanceCounterConfiguration><PerformanceCounterConfiguration counterSpecifier=\"\\PhysicalDisk\\ReadsPerSecond\" sampleRate=\"PT15S\" unit=\"CountPerSecond\"><annotation displayName=\"Disk reads\" locale=\"en-us\"/></PerformanceCounterConfiguration><PerformanceCounterConfiguration counterSpecifier=\"\\PhysicalDisk\\WritesPerSecond\" sampleRate=\"PT15S\" unit=\"CountPerSecond\"><annotation displayName=\"Disk writes\" locale=\"en-us\"/></PerformanceCounterConfiguration><PerformanceCounterConfiguration counterSpecifier=\"\\PhysicalDisk\\AverageReadTime\" sampleRate=\"PT15S\" unit=\"Seconds\"><annotation displayName=\"Disk read time\" locale=\"en-us\"/></PerformanceCounterConfiguration><PerformanceCounterConfiguration counterSpecifier=\"\\PhysicalDisk\\AverageWriteTime\" sampleRate=\"PT15S\" unit=\"Seconds\"><annotation displayName=\"Disk write time\" locale=\"en-us\"/></PerformanceCounterConfiguration><PerformanceCounterConfiguration counterSpecifier=\"\\PhysicalDisk\\AverageTransferTime\" sampleRate=\"PT15S\" unit=\"Seconds\"><annotation displayName=\"Disk transfer time\" locale=\"en-us\"/></PerformanceCounterConfiguration><PerformanceCounterConfiguration counterSpecifier=\"\\PhysicalDisk\\AverageDiskQueueLength\" sampleRate=\"PT15S\" unit=\"Count\"><annotation displayName=\"Disk queue length\" locale=\"en-us\"/></PerformanceCounterConfiguration></PerformanceCounters>"
  },
  "resources": [
    {
      "type": "Microsoft.Network/publicIPAddresses",
      "name": "[variables('publicIPAddressName')]",
      "location": "[resourceGroup().location]",
      "apiVersion": "[variables('networkApiVersion')]",
      "properties": {
        "publicIPAllocationMethod": "Dynamic",
        "dnsSettings": {
          "domainNameLabel": "[variables('longNamingInfix')]"
        }
      }
    },
    {
      "type": "Microsoft.Network/loadBalancers",
      "name": "[variables('loadBalancerName')]",
      "location": "[resourceGroup().location]",
      "apiVersion": "[variables('networkApiVersion')]",
      "dependsOn": [
        "[concat('Microsoft.Network/publicIPAddresses/', variables('publicIPAddressName'))]"
      ],
      "properties": {
        "frontendIPConfigurations": [
          {
            "name": "LoadBalancerFrontEnd",
            "properties": {
              "publicIPAddress": {
                "id": "[variables('publicIPAddressID')]"
              }
            }
          }
        ],
        "backendAddressPools": [
          {
            "name": "[variables('bePoolName')]"
          }
        ],
        "loadBalancingRules": [
          {
            "name": "LBRule",
            "properties": {
              "frontendIPConfiguration": {
                "id": "[variables('frontEndIPConfigID')]"
              },
              "backendAddressPool": {
                "id": "[variables('lbPoolID')]"
              },
              "protocol": "tcp",
              "frontendPort": 80,
              "backendPort": 80,
              "enableFloatingIP": false,
              "idleTimeoutInMinutes": 5,
              "probe": {
                "id": "[variables('lbProbeID')]"
              }
            }
          }
        ],
        "probes": [
          {
            "name": "tcpProbe",
            "properties": {
              "protocol": "tcp",
              "port": 80,
              "intervalInSeconds": "5",
              "numberOfProbes": "2"
            }
          }
        ],
        "inboundNatPools": [
          {
            "name": "[variables('natPoolName')]",
            "properties": {
              "frontendIPConfiguration": {
                "id": "[variables('frontEndIPConfigID')]"
              },
              "protocol": "tcp",
              "frontendPortRangeStart": "[variables('natStartPort')]",
              "frontendPortRangeEnd": "[variables('natEndPort')]",
              "backendPort": "[variables('natBackendPort')]"
            }
          }
        ]
      }
    },

    
    {
      "type": "Microsoft.Compute/virtualMachineScaleSets",
      "name": "[variables('namingInfix')]",
      "location": "[resourceGroup().location]",
      "apiVersion": "2016-04-30-preview",
      "dependsOn": [
        "[concat('Microsoft.Network/loadBalancers/', variables('loadBalancerName'))]"
      ],
      "sku": {
        "name": "[parameters('vmSku')]",
        "tier": "Standard",
        "capacity": "[parameters('instanceCount')]"
      },
      "properties": {
        "overprovision": "true",
        "upgradePolicy": {
          "mode": "Manual"
        },
        "virtualMachineProfile": {
          "storageProfile": {
            "osDisk": {
              "caching": "ReadOnly",
              "createOption": "FromImage"
            },
            "imageReference": "[variables('imageReference')]"
          },
          "osProfile": {
            "computerNamePrefix": "[variables('namingInfix')]",
            "adminUsername": "[parameters('adminUsername')]",
            "adminPassword": "[parameters('adminPassword')]",
            "linuxConfiguration": {
              "disablePasswordAuthentication": "false",
              "ssh": {
                "publicKeys": [
                  {
                    "path": "[variables('sshKeyPath')]",
                    "keyData": "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAgEArje2e9fbxRd9J9U6yGJ6wfEYvTylTseanQhNgwuTeS5Lj3e9r3J5tohY3P57wPRba1/CUoBw08hvPmFFm+WH6YLZ+o7hUFJ0+fTcbeNIdRxhw0H2aPR8te47EnmV98DgbL+8bGYAxGWKovfw39ssxeiuY/fX9LGnRp8OacNjP5bXKeubbRUvj0dQ0g6EHvy4G/b+UHLhoCCdvnnjufn7Eet9hOYlQy1CQy9CpqRi+AxGu8GJ/hG5OKwv/KWeoBO59SZIijqoyPzuBH8oWcSn09lOmq133LXztUuGqCDutnf2coEYGUCJpAbrjpEe0G3wVzqmhZ0qErp4EkNLVz9pzjCfbHi7hQYxK1PNNdF7PiITVS/riPQCIPOnd6oYY2cdmWbaPH1slgavfXcnbOpQPT0XGrzFQ/VSZsgGMc4YI1NJAtTPRPtCo0KmYysQjV/GhE+FeCORnIdkGg/mq9QI9BneECikOvNF8iaWlVqP8MejaqbXGRaGE4iZJzqeSRfU0j6NcKDN0AoiMUuTOnPPrXJXNnLjGy9wH39cpOHk4PjJSmfxanag8lkr5x851GvjFQ4uUIIB05HbrEoSumcEVXCcA6Hq4vc8qC1HiGXRT+1Nddce/4Psy66CLByZEgKH2STKliSQ5HiNCaefIU/dnYwinaIUV94GCIcn8DLnQWk= zied@xenserver2"
                  },
                  {
                    "path": "[variables('sshKeyPath')]",
                    "keyData": "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEApp46cCc0x58lfe9xRDd1jZdx4PXEyUCfX5z+a4j9RkB5PCMbn3i+fgmtTe0EaHHdIj1L0JGPUKpHOoJFhqoP9Vg0FaNR8t4u4fzjIjGDGpf8VflFryWyo/pCAQxzeP0WB44rnmBGqDfR9acSVH1dIG+MFLrKfYmYI2y6kcOJH7DRp8hfxt0ekB/yhiamAOtnT/mUglBrnLvvp6oc+iiOdIJ4Ohj8xCjM8gKPoQjWoGmQokOsuUUE/wXvoeCdSLCdXKo9UDcSmzHxe7JG0A2KqKwVAliMElKv7JcoKQU5gCpSm9iT6WbqkSn3w7ZeezWU+imC9ye8nCaA+XxFeIMCHw== rsa-key-20151207Ali"
                  },
                  {
                    "path": "[variables('sshKeyPath')]",
                    "keyData": "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAtUlkmltveNiQq0AdnZB/EgbUESlf4rcyowXnB3ZD/hPYHt64PIj28QiMTm3RKpHu9dQ/3tUjEzoOWboAfucbipDA5KcMF0TKNqOOcccX9IMkOMUCM7zdAOAUPNWhaFOwPqmofEKTCtNABY5GQvVLa6or+BEP6ElF6b7s/AzyUS0n+gFPXNUWEr2WUadWLd9bBoNBQcjH25Okbe2tCKe+FD1SS8QBYNxUVhX7PNp8T/YOdh/PkQ8GWSf9o3TX7r3kCmDllz5la9vRcn6tCoD0pTNrzlMPagmoMMNYSiQbprv4KhOhKqWaZgNyHd47RZcTnLnQj7ev0VwyxhntWUXQyQ== root@MEDIA"
                  }
                ]
              }
            }
          },
          "networkProfile": {
            "networkInterfaceConfigurations": [
              {
                "name": "[variables('nicName')]",
                "properties": {
                  "primary": "true",
                  "ipConfigurations": [
                    {
                      "name": "[variables('ipConfigName')]",
                      "properties": {
                        "subnet": {
                          "id": "[concat('/subscriptions/', subscription().subscriptionId,'/resourceGroups/', resourceGroup().name, '/providers/Microsoft.Network/virtualNetworks/', variables('virtualNetworkName'), '/subnets/', variables('mnSubnetName'))]"
                        },
                        "loadBalancerBackendAddressPools": [
                          {
                            "id": "[concat('/subscriptions/', subscription().subscriptionId,'/resourceGroups/', resourceGroup().name, '/providers/Microsoft.Network/loadBalancers/', variables('loadBalancerName'), '/backendAddressPools/', variables('bePoolName'))]"
                          }
                        ],
                        "loadBalancerInboundNatPools": [
                          {
                            "id": "[concat('/subscriptions/', subscription().subscriptionId,'/resourceGroups/', resourceGroup().name, '/providers/Microsoft.Network/loadBalancers/', variables('loadBalancerName'), '/inboundNatPools/', variables('natPoolName'))]"
                          }
                        ]
                      }
                    }
                  ]
                }
              }
            ]
          },
          "extensionProfile": {
            "extensions": [
              {
                "name": "drupalextension",
                "properties": {
                  "publisher": "Microsoft.Azure.Extensions",
                  "type": "CustomScript",
                  "typeHandlerVersion": "2.0",
                  "autoUpgradeMinorVersion": true,
                  "settings": {
                    "fileUris": 
                      [
                        "[concat(parameters('templateLocation'),'/scripts/configure_vm.sh')]",
                        "[concat(parameters('templateLocation'),'/conf/apache/000-default.conf')]",
                        "[concat(parameters('templateLocation'),'/conf/apache/15-portail.tunisiatv.tn.conf')]",
                        "[concat(parameters('templateLocation'),'/conf/apache/20-sport.tunisiatv.tn.conf')]",
                        "[concat(parameters('templateLocation'),'/conf/apache/25-akhbareddar.tunisiatv.tn.conf')]",
                        "[concat(parameters('templateLocation'),'/conf/apache/30-news.tunisiatv.tn.conf')]",
                        "[concat(parameters('templateLocation'),'/conf/update_akhbardar.sh')]",
                        "[concat(parameters('templateLocation'),'/conf/update_news.sh')]",
                        "[concat(parameters('templateLocation'),'/conf/update_sport.sh')]",
                        "[concat(parameters('templateLocation'),'/conf/update_watania.sh')]",
                        "[concat(parameters('templateLocation'),'/conf/apache/apache2.conf')]",
                        "[concat(parameters('templateLocation'),'/conf/apache/security.conf')]",
                        "[concat(parameters('templateLocation'),'/conf/apache/php-fpm.conf')]",
                        "[concat(parameters('templateLocation'),'/conf/apache/fpmwww.conf')]",
                        "[concat(parameters('templateLocation'),'/conf/apache/fastcgi.conf')]",
                        "[concat(parameters('templateLocation'),'/conf/varnish/default.vcl')]",
                        "[concat(parameters('templateLocation'),'/conf/varnish/varnish.service')]",
                        "[concat(parameters('templateLocation'),'/conf/varnish/varnishncsa.service')]",
                        "[concat(parameters('templateLocation'),'/conf/varnish/VarnishLogtailer.py')]"
                      ],
                      "commandToExecute":
                      
                      "[concat('sudo bash configure_vm.sh  -d ',parameters('databaseHost'))]"
                  }
                }
              }
			]
		  }
        }
      }
    }

  ]
}